## About

- I used **Selenium webdriver** for automation, **Mocha** as a test runner and **Chai** as an assertion library.
Tests are written in **TypeScript**.


## Prerequisites

- **Node.js** installed on your machine (you can get it [here](https://nodejs.org/en/))
- **chromedriver** that matches your Chrome version (you can get it [here](http://chromedriver.storage.googleapis.com/index.html))
- Directory where **chromedriver** is located on your machine should be added to your **PATH** environment variable


## Running the test

- Clone the repo
  ```
  git clone https://gitlab.com/tinalaketic/lovely-stuff-tests.git
  ```
- CD into it
  ```
  cd lovely-stuff-tests
  ```
- Install node dependecies
  ```bash
  npm install
  ```
- Run the test
  ```bash
  npm start
  ```
