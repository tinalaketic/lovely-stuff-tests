import { By, WebDriver } from 'selenium-webdriver';

export class ProductPage {
  private readonly driver: WebDriver;

  constructor(driver: WebDriver) {
    this.driver = driver;
  }

  clickViewCartButton() {
     this.driver.switchTo().frame(this.getCartPopupIframe())
     this.getViewCartButton().click()
     this.driver.switchTo().defaultContent()
  }

  getAddToCartButton() {
    return this.driver.findElement(By.css('[data-hook="add-to-cart"]'))
  }

  getCartPopupIframe() {
    return this.driver.findElement(By.css("iframe._2DJg7"))
  }

  getViewCartButton() {
    return this.driver.findElement(By.css('[data-hook="widget-view-cart-button"]'))
  }
}
