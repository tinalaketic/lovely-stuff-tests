import { By, WebDriver } from 'selenium-webdriver';

export class CartPage {
  private readonly driver: WebDriver;

  constructor(driver: WebDriver) {
    this.driver = driver;
  }

  clickCheckoutButton() {
    return this.driver.findElement(By.css('[data-hook="CheckoutButtonDataHook.button"]')).click()
  }

  getDialogTitleText() {
    this.driver.switchTo().frame(this.getCheckoutPopupIframe())
    return this.getDialogTitleText()
  }

  getCheckoutPopupIframe() {
    return this.driver.findElement(By.css("iframe.MUsGO"))
  }
}
