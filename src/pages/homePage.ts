import { By, WebDriver } from 'selenium-webdriver';

export class HomePage {
  private readonly url = 'https://lovelytests.wixsite.com/stuff';
  private readonly driver: WebDriver;

  constructor(driver: WebDriver) {
    this.driver = driver;
  }

  async visit() {
    await this.driver.get(this.url);
  }

  getProductByName(productName) {
    return this.driver.findElement(By.xpath(`//*[@data-hook="product-item-name"][contains(text(),'${productName}')]`))
  }
}
