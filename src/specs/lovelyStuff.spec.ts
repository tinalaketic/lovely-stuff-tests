import { Builder } from 'selenium-webdriver'
import { assert } from 'chai'
import { HomePage } from '../pages/homePage'
import { ProductPage } from '../pages/productPage'
import { CartPage } from '../pages/cartPage'

const driver = new Builder().forBrowser('chrome').build()
const homePage = new HomePage(driver)
const productPage = new ProductPage(driver)
const cartPage = new CartPage(driver)

after(async function() {
  await driver.quit()
})

describe('Lovely stuff', function() {
  it('buys a product', async function() {    
    this.timeout(60 * 1000)
    
    await homePage.visit()
    assert.equal(await driver.getTitle(), 'Home | Lovely Stuff')

    await homePage.getProductByName('Lipstick').click()
    await driver.sleep(10000)
    await productPage.getAddToCartButton().click()
    await driver.sleep(10000)
    await productPage.clickViewCartButton()
    await driver.sleep(10000)
    await cartPage.clickCheckoutButton()
    await driver.sleep(10000)
    assert.equal(await cartPage.getDialogTitleText(), 
                 "We can't accept online orders right now");
  })
})
